# audioplayers_aurora

The Aurora implementation of [audioplayers](https://pub.dev/packages/audioplayers) (used directly by gstreamer-1.0 without QT dependencies).


## Attention!!!

Don't forget to install aurora psdk gstreamer 1.0. This package is not installed by default in the aurora sdk, but it is necessary to build the plugin. But gstreamer 1.0 is present in Aurora OS on devices.

You can do this with the following command:

`aurora_psdk sb2 -t Aurora_OS-4.0.2.249-armv7hl.default -m sdk-install -R zypper in gstreamer1.0-plugins-base-devel`

## Usage

This package is not an _endorsed_ implementation of `audioplayers`.
Therefore, you have to include `audioplayers_aurora` alongside `audioplayers` as dependencies in your `pubspec.yaml` file.

***.desktop**

```desktop
Permissions=Audio
```

***.spec**

```spec
%global __requires_exclude ^lib(dconf|flutter-embedder|maliit-glib|gstreamer-1.0|.+_platform_plugin)\\.so.*$
```

**pubspec.yaml**

```yaml
dependencies:
  audioplayers: ^5.2.1
  audioplayers_aurora:
    git:
      url: https://gitlab.com/alexsherkhan/audioplayers_aurora_gs.git
      ref: main
      path: ./
```

