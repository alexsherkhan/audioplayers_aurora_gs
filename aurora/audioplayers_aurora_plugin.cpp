#include <audioplayers_aurora/audioplayers_aurora_plugin.h>
#include <flutter/method-channel.h>
#include <flutter/method-codec-type.h>
#include <flutter/method-response.h>
#include <sys/utsname.h>

void AudioplayersAuroraPlugin::RegisterWithRegistrar(
    PluginRegistrar& registrar) {
  MethodCodecType methodCodec = MethodCodecType::Standard;
  registrar.RegisterMethodChannel(
      "xyz.luan/audioplayers", methodCodec,
      [this](const MethodCall& call) { this->onMethodCall(call); });

  MethodCodecType globalMethodCodec = MethodCodecType::Standard;
  registrar.RegisterMethodChannel(
      "xyz.luan/audioplayers.global", globalMethodCodec,
      [this](const MethodCall& call) { this->onMethodCallGlobal(call); });
}

void AudioplayersAuroraPlugin::CreatePlayer(const std::string& playerId) {
  MethodCodecType eventCodec = MethodCodecType::Standard;
  auto eventChannel = EventChannel(
      ("xyz.luan/audioplayers/events/" + playerId).c_str(), eventCodec);
  auto methodChannel =
      MethodChannel("xyz.luan/audioplayers", MethodCodecType::Standard);
  auto player = std::make_unique<AudioPlayer>(
      playerId, methodChannel,
      ("xyz.luan/audioplayers/events/" + playerId).c_str());
  audioPlayers.insert(std::make_pair(playerId, std::move(player)));
}

AudioPlayer* AudioplayersAuroraPlugin::GetPlayer(const std::string& playerId) {
  auto searchPlayer = audioPlayers.find(playerId);
  if (searchPlayer == audioPlayers.end()) {
    return nullptr;
  }
  return searchPlayer->second.get();
}

void AudioplayersAuroraPlugin::OnGlobalLog(const gchar* message) {
  Encodable::Map map =
      Encodable::Map{{"event", "audio.onLog"}, {"value", message}};
  EventChannel("xyz.luan/audioplayers.global/events", MethodCodecType::Standard)
      .SendEvent(map);
}

void AudioplayersAuroraPlugin::HandleGlobalMethodCall(
    const MethodCall& method_call) {
  MethodResponse response = MethodResponse();
  int result = 1;
  const std::string method = method_call.GetMethod();

  if (method.compare("setAudioContext") == 0) {
    OnGlobalLog("Setting AudioContext is not supported on Linux");
  } else if (method.compare("emitLog") == 0) {
    auto flMessage = method_call.GetArgument<Encodable::String>("message");
    auto message = &(flMessage) == nullptr ? "" : flMessage;
    OnGlobalLog(message.c_str());
  } else if (method.compare("emitError") == 0) {
    auto flCode = method_call.GetArgument<Encodable::String>("code");
    auto code = &(flCode) == nullptr ? "" : flCode;
    auto flMessage = method_call.GetArgument<Encodable::String>("message");
    auto message = &(flMessage) == nullptr ? "" : flMessage;
    EventChannel("xyz.luan/audioplayers.global/events",
                 MethodCodecType::Standard)
        .SendError(code, message, nullptr);
  } else {
    response = MethodResponse();
    method_call.SendSuccessResponse(response.GetResult());
    return;
  }

  response = MethodResponse(Encodable::Int(result));
  method_call.SendSuccessResponse(response.GetResult());
}

void AudioplayersAuroraPlugin::HandleMethodCall(const MethodCall& method_call) {
  MethodResponse response = MethodResponse();
  const std::string method = method_call.GetMethod();

  const auto flPlayerId =
      method_call.GetArgument<Encodable::String>("playerId");
  if (&(flPlayerId) == nullptr) {
    response =
        MethodResponse("AuroraAudioError",
                       "Call missing mandatory parameter playerId.", nullptr);
    method_call.SendErrorResponse(response.GetErrorCode(),
                                  response.GetErrorMessage(),
                                  response.GetErrorDetails());
    return;
  }
  auto playerId = std::string(flPlayerId);

  if (method.compare("create") == 0) {
    CreatePlayer(playerId);
    response = MethodResponse(Encodable::Int(1));
    method_call.SendSuccessResponse(response.GetResult());
    return;
  }

  auto player = GetPlayer(playerId);
  if (!player) {
    response = MethodResponse(
        "AuroraAudioError",
        "Player has not yet been created or has already been disposed.",
        nullptr);
    method_call.SendErrorResponse(response.GetErrorCode(),
                                  response.GetErrorMessage(),
                                  response.GetErrorDetails());
    return;
  }

  Encodable result = Encodable::Null(nullptr);

  try {
    if (method.compare("pause") == 0) {
      player->Pause();
    } else if (method.compare("resume") == 0) {
      player->Resume();
    } else if (method.compare("stop") == 0) {
      player->Stop();
    } else if (method.compare("release") == 0) {
      player->ReleaseMediaSource();
    } else if (method.compare("seek") == 0) {
      auto flPosition = method_call.GetArgument<Encodable::Int>("position");
      int position =
          &(flPosition) == nullptr ? (int)(player->GetPosition().value_or(0)) : flPosition;
      player->SetPosition(position);
    } else if (method.compare("setSourceUrl") == 0) {
      auto flUrl = method_call.GetArgument<Encodable::String>("url");
      if (&(flUrl) == nullptr) {
        response = MethodResponse(
            "AuroraAudioError", "Null URL received on setSourceUrl.", nullptr);
        method_call.SendErrorResponse(response.GetErrorCode(),
                                      response.GetErrorMessage(),
                                      response.GetErrorDetails());
        return;
      }
      auto url = std::string(flUrl);

      auto flIsLocal = method_call.GetArgument<Encodable::Boolean>("isLocal");
      bool isLocal = &(flIsLocal) == nullptr ? false : flIsLocal;
      if (isLocal) {
        url = std::string("file://") + url;
      }
      player->SetSourceUrl(url);
    } else if (method.compare("getDuration") == 0) {
      auto optDuration = player->GetDuration();
      result = optDuration.has_value() ? Encodable::Int(optDuration.value())
                                       : 0;
    } else if (method.compare("setVolume") == 0) {
      auto flVolume = method_call.GetArgument<Encodable::Float>("volume");
      double volume = &(flVolume) == nullptr ? 1.0 : flVolume;
      player->SetVolume(volume);
    } else if (method.compare("getCurrentPosition") == 0) {
      auto optPosition = player->GetPosition();
      result = optPosition.has_value() ? Encodable::Int(optPosition.value())
                                       : 0;
    } else if (method.compare("setPlaybackRate") == 0) {
      auto flPlaybackRate =
          method_call.GetArgument<Encodable::Float>("playbackRate");
      double playbackRate = &(flPlaybackRate) == nullptr ? 1.0 : flPlaybackRate;
      player->SetPlaybackRate(playbackRate);
    } else if (method.compare("setReleaseMode") == 0) {
      auto flReleaseMode =
          method_call.GetArgument<Encodable::String>("releaseMode");
      std::string releaseMode = &(flReleaseMode) == nullptr
                                    ? std::string()
                                    : std::string(flReleaseMode);
      if (releaseMode.empty()) {
        response = MethodResponse(
            "AuroraAudioError",
            "Error calling setReleaseMode, releaseMode cannot be null",
            nullptr);
        method_call.SendErrorResponse(response.GetErrorCode(),
                                      response.GetErrorMessage(),
                                      response.GetErrorDetails());
        return;
      }
      auto looping = releaseMode.find("loop") != std::string::npos;
      player->SetLooping(looping);
    } else if (method.compare("setPlayerMode") == 0) {
      // TODO check support for low latency mode:
      // https://gstreamer.freedesktop.org/documentation/additional/design/latency.html?gi-language=c
      unimplemented(method_call);
    } else if (method.compare("setBalance") == 0) {
      auto flBalance = method_call.GetArgument<Encodable::Float>("balance");
      double balance = &(flBalance) == nullptr ? 0.0f : flBalance;
      player->SetBalance(balance);
    } else if (method.compare("emitLog") == 0) {
      auto flMessage = method_call.GetArgument<Encodable::String>("message");
      auto message = &(flMessage) == nullptr ? "" : flMessage;
      player->OnLog(message.c_str());
    } else if (method.compare("emitError") == 0) {
      auto flCode = method_call.GetArgument<Encodable::String>("code");
      auto code = &(flCode) == nullptr ? "" : flCode;
      auto flMessage = method_call.GetArgument<Encodable::String>("message");
      auto message = &(flMessage) == nullptr ? "" : flMessage;
      Encodable details = Encodable::Null(nullptr);
      player->OnError(code.c_str(), message.c_str(), details);
    } else if (method.compare("dispose") == 0) {
      player->Dispose();
      audioPlayers.erase(playerId);
    } else {
      response = MethodResponse();
      method_call.SendSuccessResponse(response.GetResult());
      return;
    }

    response = MethodResponse(result);
    method_call.SendSuccessResponse(response.GetResult());
  } catch (const gchar* error) {
    response = MethodResponse("AuroraAudioError", error, nullptr);
    method_call.SendErrorResponse(response.GetErrorCode(),
                                  response.GetErrorMessage(),
                                  response.GetErrorDetails());
  } catch (...) {
    std::exception_ptr p = std::current_exception();
    response = MethodResponse("AuroraAudioError",
                              p ? p.__cxa_exception_type()->name()
                                : "Unknown AudioPlayersLinux error",
                              nullptr);
    method_call.SendErrorResponse(response.GetErrorCode(),
                                  response.GetErrorMessage(),
                                  response.GetErrorDetails());
  }
}

void AudioplayersAuroraPlugin::Dispose() {
  for (const auto& entry : audioPlayers) {
    entry.second->Dispose();
  }
  gst_deinit();
}

void AudioplayersAuroraPlugin::onMethodCall(const MethodCall& call) {
  HandleMethodCall(call);
}

void AudioplayersAuroraPlugin::onMethodCallGlobal(const MethodCall& call) {
  HandleGlobalMethodCall(call);
}

void AudioplayersAuroraPlugin::unimplemented(const MethodCall& call) {
  call.SendSuccessResponse(nullptr);
}
